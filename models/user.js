const mongoose = require("mongoose");


const userSchema = mongoose.Schema({
    firstName: {
        type: String,
        required: [true, "Please provide your first name"]
    },
    lastName: {
        type: String,
        required: [true, "Please provide your last name"]
    },
    mobileNo: {
        type: String,
        required: [true, "Please provide your mobile number"]
    },
    email: {
        type: String,
        required: [true, "Please provide your email"]
    },
    password: {
        type: String,
        required: [true, "Password is required!"]
    },
    
    // address: 
    //     {
    //         houseNo: {
    //             type: Number
    //         },
    //         street: {
    //             type: String
    //         },
    //         barangay: {
    //             type: String,
    //             //required: [true, "Please specify your barangay"]
    //         },
    //         city: {
    //             type: String,
    //             //required: [true, "Please specify your municipality or city."]
    //         },
    //         province: {
    //             type: String,
    //            // required: [true, "Please specify your province."]
    //         }
    //     },

    isAdmin: {
        type: Boolean,
        default: false
    },
    cart : [
        {
            productId : {
                type : String,
                required : [true, "Product ID is required"]
            },
            quantity : {
                type : Number,
                default : 1
            },
            // total: {
            //     type: Number
            // },
            orderedOn : {
                type : Date,
                default : new Date()
            }
        }
    ]
});

module.exports = mongoose.model("User", userSchema);