const User = require("../models/user.js");
const Product = require("../models/product.js")

const bcrypt = require("bcrypt");
const auth = require("../auth.js")


module.exports.registerUser = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0){
			return `Email is already registered. Please login.`
		}
		else{

			let newUser = new User({
				firstName : reqBody.firstName,
				lastName: reqBody.lastName,
				email: reqBody.email,

				password: bcrypt.hashSync(reqBody.password, 10),
				mobileNo: reqBody.mobileNo
				// address:{
				// 	houseNo: reqBody.houseNo,
				// 	street: reqBody.street,
				// 	barangay: reqBody.barangay,
				// 	city: reqBody.city,
				// 	province: reqBody.province
				// }
			})

			return newUser.save().then((user, error) => {
				if(error){
					return false;
				}
				else{
					newUser.password = "*****";
					return newUser;
				}
			})
		}
	})
}


module.exports.userLogin = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		if(result == null){
			return `Email not registered. Please create an account`;
		}
		else{
			const verifiedPassword = bcrypt.compareSync(reqBody.password, result.password);

			if(verifiedPassword){
				//return {access: auth.createAccessToken(result)};
				if(result.isAdmin == true){
					return `Logged in successfully! Welcome, Admin ${result.firstName}!`;
				}
				else if(result.isAdmin == false){
					return `Logged in successfully! Welcome, ${result.firstName}!`;
				}
			}
			else{
				return "Incorrect password";
			}
		}
	})
}

module.exports.getAllUsers = () => {
		return User.find({}).then(result => {
		return result;
		});
};

module.exports.userDetails = (userInfo) => {
	return User.findById(userInfo).then((details, err) => {
		if(err){
			return false;
		}
		else{
			//details.password = "*****";
			return details;
		}
	})
};
module.exports.createOrder = async (data) => {

    let isUserUpdated = await User.findById(data.userId).then(user => {
        user.cart.push({productId : data.productId});

        return user.save().then((user, error) => {
            if(error){
                return false;
            }else{
                return true;
            }
        })

    })

    let isProductUpdated = await Product.findById(data.productId).then(product => {
        product.orders.push({userId : data.userId});

        return product.save().then((product, error) => {
            if(error){
                return false
            }else{
                return true;
            }
        })
    })

    if(isUserUpdated && isProductUpdated){
        return `Item added to cart`;
    }else{
        return false;
    }
}


module.exports.makeAdmin = (userId) => {
	const changeRole = {
		isAdmin: true
	}

	return User.findByIdAndUpdate(userId, changeRole).then((user, error) => {
	if(error){
		return false
	}
	else{
		return{message:`${user.firstName} has been added as Admin.`}
	};
	});
};