const mongoose = require("mongoose");
const Product = require("../models/product.js");

module.exports.addProduct = (data) => {
	console.log(data.isAdmin)

	if(data.isAdmin) {
		let newProduct = new Product({
			name: data.product.name,
			description: data.product.description,
			price: data.product.price
		});

		return newProduct.save().then((newProduct, error) => {
			if(error){
				return error;
			}
				return newProduct
		})
	};

		let message = Promise.resolve('Admin Access is required.')
			return message.then((value) => {
				{return value}
			})
	
};

module.exports.getAllProduct = () => {
	return Product.find({}).then(result => {
		return result;
	})
}

module.exports.getActiveProducts = () => {
	return Product.find({isActive:true}).then(result => {
		return result;
	})
}

module.exports.getProduct = (productId) => {
	return Product.findById(productId).then(result => {
		return result;
	})
}

module.exports.updateProduct = (productId, newData) => {
	if(newData.isAdmin == true){
		return Product.findByIdAndUpdate(productId, {
			name: newData.product.name,
			description: newData.product.description,
			price: newData.product.price
		}).then((updatedProduct, error) => {
			if(error){
				return false
			}
				return updatedProduct
		})
	}
	else{
		let message = Promise.resolve('Admin Access is Required.');
			return message.then((value) => {return value})
	}
}

module.exports.archiveProduct = (productId) => {
	const changeStatus = {
		isActive: false
	}

	return Product.findByIdAndUpdate(productId, changeStatus).then((product, error) => {
	if(error){
		return false
	}
	else{
		return{message:"Product archived successfully!"}
	};
	});
};