const express = require("express");
const router = express.Router();
const User = require("../models/user.js");
const userController = require("../controllers/userController.js");
const auth = require("../auth.js");


router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(result =>
		res.send(result));
});

router.post("/login", (req, res) => {
	userController.userLogin(req.body).then(result => 
		res.send(result));
});

router.get("/allUsers", auth. verify, (req, res) => {
	if(auth.decode(req.headers.authorization).isAdmin === false){
		res.send("Admin Access is required.")
	}
	else{
	userController.getAllUsers().then(result => res.send(result));
	}
});

router.get("/:userId", auth.verify, (req, res) => {
	userController.userDetails(req.params.userId).then(result => res.send(result));
});

router.patch("/:userId/addAdmin", auth.verify, (req, res) => {
	if(auth.decode(req.headers.authorization).isAdmin === false){
		res.send("Admin Access is required.");
	}
	else{
		userController.makeAdmin(req.params.userId).then(result => res.send(result))
	}
});

router.post("/order", auth.verify, (req, res) => {
	if(auth.decode(req.headers.authorization).isAdmin === true){
		res.send("Unauthorized access.")
	}
	else{
		let data = {
			userId: auth.decode(req.headers.authorization).id,
			productId: req.body.productId
		}

	userController.createOrder(data).then(result => res.send(result));
}});


module.exports = router;