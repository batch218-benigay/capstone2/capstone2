const express = require("express");
const router = express.Router();

const productController = require("../controllers/productController.js");
const auth = require("../auth.js");


router.post("/createProduct", auth.verify, (req, res) => {
		if(auth.decode(req.headers.authorization).isAdmin === false){
		res.send("Admin Access is required.");
	}
	else{
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.addProduct(data).then(result => res.send(result))
}});

router.get("/allProduct", auth.verify, (req, res) => {
		if(auth.decode(req.headers.authorization).isAdmin === false){
		res.send("Admin Access is required.");
	}
	else{
	productController.getAllProduct().then(result => res.send(result))
}});


router.get("/productList", auth.verify, (req, res) => {
	productController.getActiveProducts().then(result => res.send(result))
});

router.get("/:productId", auth.verify, (req, res) => {
	productController.getProduct(req.params.productId).then(result => res.send(result))
});

router.patch("/:productId/update", auth.verify, (req, res) => {
		if(auth.decode(req.headers.authorization).isAdmin === false){
		res.send("Admin Access is required.");
	}
	else{
	const newData = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.updateProduct(req.params.productId, newData).then(result => {
		res.send(result)
	})
}})

router.patch("/:productId/archive", auth.verify, (req, res) => {
	if(auth.decode(req.headers.authorization).isAdmin === false){
		res.send("Admin Access is required.");
	}
	else{
		productController.archiveProduct(req.params.productId).then(result => res.send(result))
	}
});


module.exports = router;