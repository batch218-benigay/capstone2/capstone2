const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoute = require("./routes/userRoute.js");
const productRoute = require("./routes/productRoute.js")


const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/users", userRoute);
app.use("/products", productRoute);


mongoose.connect("mongodb+srv://admin:admin@ecommerce.fbn8a4k.mongodb.net/e-commerce?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});


mongoose.connection.once('open', () => console.log('Connected to Capstone 2 Database'));

app.listen(process.env.PORT || 5000, () =>
	{console.log(`API is now accessible at port ${process.env.PORT || 5000}`)});